import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardGrid, Label as PFLabel, SparklineChart, Spinner } from 'patternfly-react';
import { connect, reduxActions } from '../../redux';
import helpers from '../../common/helpers';
import graphHelpers from '../../common/graphHelpers';

class AccountViewInstanceGraphs extends React.Component {
  componentDidMount() {
    const { filter, getAccountInstances } = this.props;

    if (filter.query) {
      getAccountInstances(filter.query);
    }
  }

  componentDidUpdate(prevProps) {
    const { filter, getAccountInstances, updateInstances } = this.props;

    if (updateInstances === true && updateInstances !== prevProps.updateInstances) {
      getAccountInstances(filter.query);
    }
  }

  renderPendingCard() {
    const { pending } = this.props;

    if (pending) {
      return (
        <Card matchHeight accented className="cloudmeter-utilization-graph cloudmeter-utilization-graph-loading fadein">
          <Card.Body>
            <Spinner loading size="sm" className="blank-slate-pf-icon" />
            <div className="text-center">
              <small>Loading...</small>
            </div>
          </Card.Body>
        </Card>
      );
    }

    return null;
  }

  render() {
    const { error, fulfilled, dailyUsage, instancesOpenshift, instancesRhel } = this.props;
    let displayTotals = {};
    let chartData = null;

    if (error) {
      return null;
    }

    if (dailyUsage && dailyUsage.length) {
      displayTotals = { ...graphHelpers.calculateInstanceTotals(dailyUsage) };
      chartData = graphHelpers.convertGraphData(dailyUsage);
    }

    return (
      <CardGrid fluid matchHeight>
        <CardGrid.Row className="row-cards-pf">
          <CardGrid.Col sm={6} md={4}>
            {this.renderPendingCard()}
            {fulfilled && (
              <Card matchHeight accented className="cloudmeter-utilization-graph fadein">
                <Card.Heading>
                  <Card.Title>Red Hat Enterprise Linux</Card.Title>
                </Card.Heading>
                <Card.Body>
                  <div className="cloudmeter-card-info">
                    <strong>{instancesRhel}</strong> 
                    <PFLabel bsStyle="warning">
                      <abbr title="Red Hat Enterprise Linux">RHEL</abbr>
                    </PFLabel>{' '}
                    Instances
                  </div>

                  {chartData && (
                    // <SparklineChart
                    //   className="cloudmeter-utilization-graph-display-c3"
                    //   data={chartData.rhelData}
                    //   tooltip={graphHelpers.graphDefaults.tooltips}
                    //   axis={graphHelpers.graphDefaults.axis}
                    // />

                    // TODO: make this  line orange + blue
                    <SparklineChart
                      data={{
                        columns: [[
                          'data1',
                          80,
                          200,
                          250,
                          90,
                          2,
                          10,
                          130
                        ]], type: 'area'
                      }}
                    />
                  )}
                </Card.Body>
              </Card>
            )}
          </CardGrid.Col>
          <CardGrid.Col sm={6} md={4}>
            {this.renderPendingCard()}
            {fulfilled && (
              <Card matchHeight accented className="cloudmeter-utilization-graph fadein">
                <Card.Heading>
                  <Card.Title>Red Hat OpenShift Container Platform</Card.Title>
                </Card.Heading>
                <Card.Body>
                  <div className="cloudmeter-card-info">
                    <strong>{instancesOpenshift}</strong>
                    <PFLabel bsStyle="primary">
                      <abbr title="Red Hat OpenShift Container Platform">RHOCP</abbr>
                    </PFLabel>{' '}
                    Instances
                  </div>
                  <div className="cloudmeter-utilization-graph-display">
                    {chartData && (
                      // <SparklineChart
                      //   className="cloudmeter-utilization-graph-display-c3"
                      //   data={chartData.openshiftData}
                      //   tooltip={graphHelpers.graphDefaults.tooltips}
                      //   axis={graphHelpers.graphDefaults.axis}
                      // />

                      <SparklineChart
                        data={{
                          columns: [[
                            'data1',
                            30,
                            200,
                            250,
                            90,
                            2,
                            10,
                            130
                          ]], type: 'area'
                        }}
                      />
                    )}
                  </div>
                </Card.Body>
              </Card>
            )}
          </CardGrid.Col>
          <CardGrid.Col sm={12} md={4}>
            {this.renderPendingCard()}
            {fulfilled && (
              <Card matchHeight accented className="cloudmeter-utilization-graph fadein">
                <Card.Heading>
                  <Card.Title>Utilized hours</Card.Title>
                </Card.Heading>
                <Card.Body>
                    <div className="cloudmeter-card-info">
                      <strong>{displayTotals.rhelHours || 0}</strong> 
                      <PFLabel bsStyle="warning">
                        <abbr title="Red Hat Enterprise Linux">RHEL</abbr>
                      </PFLabel>{' '}
                      Hrs
                    </div>

                    <div className="cloudmeter-card-info">
                      <strong>{displayTotals.openshiftHours || 0}</strong>
                      <PFLabel bsStyle="primary">
                        <abbr title="Red Hat OpenShift Container Platform">RHOCP</abbr>
                      </PFLabel>{' '}
                      Hrs
                    </div>
                  <div className="cloudmeter-utilization-graph-display">
                    {chartData && (
                      // <SparklineChart
                      //   className="cloudmeter-utilization-graph-display-c3"
                      //   data={chartData.rhelOpenshiftTime}
                      //   tooltip={graphHelpers.graphDefaults.tooltips}
                      //   axis={graphHelpers.graphDefaults.axis}
                      // />
                      // TODO: make this  line orange + blue
                      <SparklineChart
                        data={{
                          columns: [[
                            'data1',
                            30,
                            200,
                            250,
                            90,
                            2,
                            10,
                            130
                          ]], type: 'area'
                        }}
                      />
                    )}
                  </div>
                </Card.Body>
              </Card>
            )}
          </CardGrid.Col>
        </CardGrid.Row>
      </CardGrid>
    );
  }
}

AccountViewInstanceGraphs.propTypes = {
  error: PropTypes.bool,
  fulfilled: PropTypes.bool,
  filter: PropTypes.shape({
    query: PropTypes.object.isRequired
  }).isRequired,
  getAccountInstances: PropTypes.func,
  dailyUsage: PropTypes.array,
  instancesOpenshift: PropTypes.number,
  instancesRhel: PropTypes.number,
  pending: PropTypes.bool,
  updateInstances: PropTypes.bool
};

AccountViewInstanceGraphs.defaultProps = {
  error: false,
  fulfilled: false,
  getAccountInstances: helpers.noop,
  dailyUsage: [],
  instancesOpenshift: 0,
  instancesRhel: 0,
  pending: false,
  updateInstances: false
};

const mapDispatchToProps = dispatch => ({
  getAccountInstances: query => dispatch(reduxActions.account.getAccountInstances(query))
});

const mapStateToProps = state => ({
  ...state.account.instances
});

const ConnectedAccountViewInstanceGraphs = connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountViewInstanceGraphs);

export { ConnectedAccountViewInstanceGraphs as default, ConnectedAccountViewInstanceGraphs, AccountViewInstanceGraphs };
